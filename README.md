
# Aqua Security Image Bakery Pipeline

## Overview

This pipeline is designed to adhere to best practices in container image security. It integrates Aqua Security's vulnerability and misconfiguration scanning, generating a Software Bill of Materials (SBOM), marking images as immutable, signing, verifying, and promoting images to a secure, immutable repository.

### Pipeline Flow Diagram (Mermaid)

```mermaid
graph TD;
    A[Build Image] --> B[Security Scan]
    B --> C[Generate SBOM]
    C --> D[Mark as Immutable]
    D --> E[Sign Image]
    E --> F[Verify]
    F --> G[Promote to Immutable Repo]
```

## Stages

### 1. Build Image

- **Purpose**: To build a new Docker image.
- **Tool**: Docker
- **Best Practice**: Using cache for faster build time.

### 2. Security Scan

- **Purpose**: To scan the newly built image for vulnerabilities and misconfigurations.
- **Tool**: Aqua Scanner
- **Best Practice**: Fail the build if critical vulnerabilities are found.

### 3. Generate SBOM

- **Purpose**: To generate a Software Bill of Materials (SBOM) for the image.
- **Tool**: Aqua Billy
- **Best Practice**: SBOM provides a complete inventory of software components in the image.

### 4. Mark as Immutable

- **Purpose**: To mark the image as immutable once it passes all security checks.
- **Tool**: Docker
- **Best Practice**: Immutable images cannot be overwritten, enhancing security.

### 5. Sign Image

- **Purpose**: To digitally sign the image.
- **Tool**: Cosign
- **Best Practice**: Ensures the integrity and origin of the image.

### 6. Verify

- **Purpose**: To verify the digital signature of the image.
- **Tool**: Cosign
- **Best Practice**: Verifies the image's integrity before moving to production.

### 7. Promote to Immutable Repo

- **Purpose**: To promote the image to a secure, immutable repository.
- **Tool**: Docker
- **Best Practice**: Only approved images are promoted, ensuring security in the production environment.

### Sequence Diagram (Mermaid)

```mermaid
sequenceDiagram
    participant Developer as Dev
    participant Build as Build Image
    participant Scan as Aqua Security Scan
    participant SBOM as Generate SBOM
    participant Immutable as Mark as Immutable
    participant Sign as Sign Image
    participant Verify as Verify
    participant Promote as Promote to Immutable Repo
    
    Dev->>Build: Push code
    Build->>Scan: Build complete
    Scan->>SBOM: Scan complete
    SBOM->>Immutable: SBOM generated
    Immutable->>Sign: Image marked
    Sign->>Verify: Image signed
    Verify->>Promote: Verification complete
    Promote-->>Dev: Image promoted
```

