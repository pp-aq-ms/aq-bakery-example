package appshield.kubernetes.KSV1100

import data.lib.kubernetes

default failPSSRestricted = false

__rego_metadata__ := {
    "id": "KSV1100",
    "avd_id": "AVD-KSV-0110",
    "title": "Enforce PSS Restricted Mode on Deployments Three",
    "version": "v1.0.0",
    "severity": "High",
    "type": "Kubernetes Security Check",
    "description": "This policy enforces the Pod Security Standards restricted mode on Kubernetes Deployments.",
    "recommended_actions": "Ensure your Deployment configurations meet the PSS restricted mode requirements."
}

__rego_input__ := {
    "combine": false,
    "selector": [
        {"type" : "kubernetes", "group": "core", "version": "v1", "kind": "pod"},
        {"type" : "kubernetes", "group": "apps", "version": "v1", "kind": "replicaset"},
        {"type" : "kubernetes", "group": "core", "version": "v1", "kind": "replicationcontroller"},
        {"type" : "kubernetes", "group": "apps", "version": "v1", "kind": "deployment"},
        {"type" : "kubernetes", "group": "apps", "version": "v1", "kind": "statefulset"},
        {"type" : "kubernetes", "group": "apps", "version": "v1", "kind": "daemonset"},
        {"type" : "kubernetes", "group": "batch", "version": "v1", "kind": "cronjob"},
        {"type" : "kubernetes", "group": "batch", "version": "v1", "kind": "job"}
    ]
}

check_pss_restricted(container) {
    securityContext := container.securityContext
    securityContext.runAsNonRoot == true
    securityContext.runAsUser >= 1000
    securityContext.allowPrivilegeEscalation == false
    capabilities := securityContext.capabilities
    capabilities.drop[_] == "ALL"
}

getNonCompliantContainers[container] {
    container := kubernetes.containers[_]
    not check_pss_restricted(container)
}

is_pss_restricted_label {
    labelKey := "pod-security.kubernetes.io/enforce"
    kubernetes.metadata.labels[labelKey] == "restricted"
    is_target_namespace
}

is_target_namespace {
    kubernetes.namespace == "infradev"
}

is_target_namespace {
    kubernetes.namespace == "prod"
}

failPSSRestricted {
    is_pss_restricted_label
    count(getNonCompliantContainers) > 0
}

deny[res] {
    failPSSRestricted

    msg := kubernetes.format(
        sprintf(
            "container %s of %s %s in %s namespace does not meet PSS restricted mode requirements",
            [getNonCompliantContainers[_].name, lower(kubernetes.kind), kubernetes.name, kubernetes.namespace]
        )
    )
    res := {
        "msg": msg,
        "id": __rego_metadata__.id,
        "title": __rego_metadata__.title,
        "severity": __rego_metadata__.severity,
        "type": __rego_metadata__.type,
    }
}
