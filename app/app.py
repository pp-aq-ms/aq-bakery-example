
from flask import Flask
import os

app = Flask(__name__)
version = os.environ.get('APP_VERSION', '1.0')

@app.route('/')
def hello_world():
    return f'Hello, this is version {version} of my app!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
